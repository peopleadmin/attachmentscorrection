﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Domain.Entities.ATS
{
    public class CorrectionData
    {
        public int ProfileRegisterPage2ID { get; set; }
        public int ApplicantProfileID { get; set; }
        public string FileType { get; set; }
        public string UploadedFileName { get; set; }
        public string SRFileType { get; set; }
    }
}
