﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Domain.Entities.ATS
{
    public class ApplicantProfile_ProfileRegisterPage2
    {
        public int ProfileRegisterPage2ID { get; set; }
        public int ApplicantProfileId { get; set; }
        public ApplicantProfile_Header ApplicantProfile { get; set; }
        public string ResumeName { get; set; }
        public string CoverLetterName { get; set; }
        public string TranscriptsName { get; set; }
        public string Custom1Name { get; set; }
        public string Custom2Name { get; set; }
        public string Custom3Name { get; set; }
    }
}
