﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Domain.Entities.ATS
{
    public class ApplicantProfile_Header
    {
        public int ApplicantProfileID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string OtherName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
