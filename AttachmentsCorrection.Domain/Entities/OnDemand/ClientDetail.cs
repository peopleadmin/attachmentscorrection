﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Domain.Entities.OnDemand
{
    public class ClientDetail
    {
        public int ClientDeploymentID { get; set; }
        public string ApplicationName { get; set; }
        public string KeyName { get; set; }
        public string Value { get; set; }
    }
}
