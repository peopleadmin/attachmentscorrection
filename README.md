# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Created for datafix as part of TE-29894. 
ApplicantProfile_ProfileRegisterPage2 table stores attachment labels. BinaryFiles table stores actual file content.

Due to Adobe flash functionality, files were getting deleted from BinaryFiles, but the labels continue to remain in ApplicantProfile_ProfileRegisterPage2. This caused the broken attachment links to appear in ATS. 
Adobe flash functionality has been disabled in TE-29894.

This datafix tool aims to delete the labels from ApplicantProfile_ProfileRegisterPage2 which dont have corresponding file content in BinaryFiles.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact