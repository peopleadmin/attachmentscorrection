﻿using AttachmentsCorrection.Domain.Entities;
using AttachmentsCorrection.Domain.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Application.Contracts.Persistence.ATS
{
    public interface IApplicantProfileRegisterPage2Repository : IAsyncRepository<ApplicantProfile_ProfileRegisterPage2>
    {
    
    }
}
