﻿using AttachmentsCorrection.Domain.Entities.ATS;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Application.Contracts.Persistence.ATS
{
    
    public interface ICorrectionDataRepository : IAsyncRepository<CorrectionData>
    {

    }
}
