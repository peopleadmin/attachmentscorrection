﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AttachmentsCorrection.Application.Contracts.Persistence.ATS
{
    public interface IAsyncRepository<T> where T: class
    {
        Task<T> GetByIdAsync(int id);
        Task<IReadOnlyList<T>> ListAllAsync();
        void UpdateAsync(T entity);
        Task SaveChangesAsync();
    }
}
