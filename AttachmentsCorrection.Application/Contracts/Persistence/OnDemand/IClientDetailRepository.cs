﻿using AttachmentsCorrection.Domain.Entities.OnDemand;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AttachmentsCorrection.Application.Contracts.Persistence.OnDemand
{
    public interface IClientDetailRepository
    {
        Task<List<ClientDetail>> GetATSConnStrings(string rawSQL);
    }
}
