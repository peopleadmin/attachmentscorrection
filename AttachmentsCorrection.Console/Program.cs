﻿using AttachmentsCorrection.Application.Contracts.Persistence.OnDemand;
using AttachmentsCorrection.Domain.Entities.ATS;
using AttachmentsCorrection.Persistence;
using AttachmentsCorrection.Persistence.Repositories.ATS;
using AttachmentsCorrection.Persistence.Repositories.OnDemand;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace AttachmentsCorrection.Console
{
    class Program
    {
        static async Task Main(string[] args)
        {
            bool result = false;
            string currentDateTime = DateTime.Now.ToString("yyyyMMdd_HHmm");
            Log.Logger = new LoggerConfiguration()
                    .WriteTo.Console()
                    .WriteTo.File($"C:\\JagsAttachmentCorrection\\Logs_{currentDateTime}.log")
                    .CreateLogger();


            IConfiguration configuration = new ConfigurationBuilder()
                           .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                           .AddEnvironmentVariables()
                           .AddCommandLine(args)
                           .Build();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection, configuration);

            var serviceProvider = serviceCollection.BuildServiceProvider();

            var logger = serviceProvider.GetService<ILogger<Program>>();
            try
            {
                System.Console.Write("THE START!");
                logger.LogInformation("THE START!");

                string getATSConnStringsSQL = configuration.GetSection("ATSConnStringSQL").Value;
                string updateData = configuration.GetSection("UpdateData").Value;
                //if (updateData.Equals("TRUE"))
                //{
                //    System.Console.Write("UpdateData = TRUE. Data will be updated. Proceed?(y/n):");
                //    char c = (char)System.Console.Read();
                //    if (!c.Equals('y'))
                //        return;

                //    logger.LogInformation("UpdateData = TRUE. Data will be updated");
                //}
                //else
                //{
                    logger.LogInformation("UpdateData DISABLED. Proceeding with data fetch!");
                //}
                result = await serviceProvider.GetService<UpdateAttachmentData>().UpdateLabels(getATSConnStringsSQL, updateData);
                logger.LogInformation("THE END!");
            }
            catch (Exception ex)
            {
                logger.LogError("ERROR OCCURRED!");
                logger.LogInformation($"EXCEPTION Message: {ex.Message}");
                logger.LogInformation($"EXCEPTION Type: {ex.GetType()}");
                logger.LogInformation($"INNER Exceptio: {ex.InnerException}");
                logger.LogInformation($"STACKTRACE: {ex.StackTrace}");
            }
            if (result)
            {
                System.Console.WriteLine("COMPLETED SUCCESSFULLY!");
                System.Console.ReadKey();
            }
            else
            {
                System.Console.WriteLine("FAILED!");
                System.Console.ReadKey();
            }
        }
        private static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //we will configure logging here
            services.AddLogging(configure => configure.AddSerilog())
                    .AddTransient<UpdateAttachmentData>();

            services.AddScoped<IClientDetailRepository, ClientDetailRepository>();

            services.AddDbContext<OnDemandDbContext>(options =>
                        options.UseSqlServer(configuration.GetConnectionString("OnDemandContext")));

            services.AddDbContext<AttachmentsCorrectionDbContext>(
                         options => options.UseSqlServer("name=ConnectionStrings:DefaultConnection"));
        }
    }

    public class UpdateAttachmentData
    {
        private readonly ILogger<UpdateAttachmentData> _logger;
        private readonly IClientDetailRepository _clientPreferencesRepository;

        public UpdateAttachmentData(ILogger<UpdateAttachmentData> logger,
            IClientDetailRepository clientPreferencesRepository)
        {
            _logger = logger;
            _clientPreferencesRepository = clientPreferencesRepository;
        }
        public async Task<bool> UpdateLabels(string getATSConnStringsSQL, string updateData)
        {
            try
            {
                //Get ATS conn strings from ClientPref from ONDEMAND
                var clients = await _clientPreferencesRepository.GetATSConnStrings(getATSConnStringsSQL);
                _logger.LogInformation($"{clients.Count} ATS Clients");
                int countC = 0;

                foreach (var client in clients)
                {
                    _logger.LogInformation($"Processing starts({++countC}/{clients.Count}) for ClientDeploymentID:{client.ClientDeploymentID}, {client.ApplicationName}");

                    var contextOptions = new DbContextOptionsBuilder<AttachmentsCorrectionDbContext>()
                                        .UseSqlServer(client.Value)
                                        .Options;

                    _logger.LogInformation($"ConnStr ClientDeploymentID:{client.ClientDeploymentID}:{client.Value}");
                    try
                    {
                        using (var context = new AttachmentsCorrectionDbContext(contextOptions))
                        {
                            CorrectionDataRepository _correctionDataRepository = new CorrectionDataRepository(context);
                            ApplicantProfileRegisterPage2Repository _applicantProfileRegisterPage2Repository = new ApplicantProfileRegisterPage2Repository(context);

                            var correctionDatas = await _correctionDataRepository.GetCorrectionData();
                            _logger.LogInformation($"Correction data ClientDeploymentID:{client.ClientDeploymentID}, count: {correctionDatas.Count}");

                            int currentProfileRegisterPage2ID = 0;
                            ApplicantProfile_ProfileRegisterPage2 toUpdateApplicantProfileRegisterPage2 = new ApplicantProfile_ProfileRegisterPage2(); // No need of initialiation. This will be reassigned with GetByIdAsync. Initiailized here to avoid PropertyInfo.SetValue error.

                            #region sample correctionDatas
                            /* Sample values of correctionDatas
                                ProfileRegisterPage2ID ApplicantProfileID   FileType        UploadedFileName        SRFileType
                                1                           1               ResumeName      SampleResum1.doc        Applicant_Resume
                                1                           1               CoverLetterName SampleCoverLetter.doc   Applicant_CoverLetter
                                1                           1               TranscriptsName SampleTrans.doc         Applicant_Transcript
                                2                           2               ResumeName      SampleResum1.doc        Applicant_Resume
                            */
                            #endregion

                            _logger.LogInformation($"Starting correction: ProfileRegisterPage2ID,ApplicantProfileID,FileType,UploadedFileName,SRFileType");
                            foreach (var correctionData in correctionDatas)
                            {
                                if (correctionData.ApplicantProfileID != 0)
                                {
                                    //_logger.LogInformation($"CorrectionDataRow:{correctionData.ProfileRegisterPage2ID},{correctionData.ApplicantProfileID}," +
                                    //    $"{correctionData.FileType},{correctionData.UploadedFileName},{correctionData.SRFileType}");

                                    Type type = typeof(ApplicantProfile_ProfileRegisterPage2);
                                    PropertyInfo propertyInfo = type.GetProperty(correctionData.FileType);

                                    if (currentProfileRegisterPage2ID != correctionData.ProfileRegisterPage2ID)
                                    {
                                        if (currentProfileRegisterPage2ID != 0)
                                        {
                                            //if (updateData.Equals("TRUE"))
                                            //{
                                            //    // No more rows for data correction with id ProfileRegisterPage2ID. Set the entity state to Modified
                                            //    _applicantProfileRegisterPage2Repository.UpdateAsync(toUpdateApplicantProfileRegisterPage2);
                                            //}
                                        }

                                        currentProfileRegisterPage2ID = correctionData.ProfileRegisterPage2ID;

                                        //Get the new record from ApplicantProfile_ProfileRegisterPage2 based on ProfileRegisterPage2ID
                                        toUpdateApplicantProfileRegisterPage2 = await _applicantProfileRegisterPage2Repository.GetByIdAsync(correctionData.ProfileRegisterPage2ID);
                                        propertyInfo.SetValue(toUpdateApplicantProfileRegisterPage2, "");
                                        _logger.LogInformation($"DataUpdate for Client:{client.ClientDeploymentID}-{client.ApplicationName} " +
                                            $"|ProfileRegisterPage2ID:{correctionData.ProfileRegisterPage2ID}| Applicant:{correctionData.ApplicantProfileID}#{toUpdateApplicantProfileRegisterPage2.ApplicantProfile.FirstName} " +
                                            $"{toUpdateApplicantProfileRegisterPage2.ApplicantProfile.MiddleName} {toUpdateApplicantProfileRegisterPage2.ApplicantProfile.OtherName} " +
                                            $"{toUpdateApplicantProfileRegisterPage2.ApplicantProfile.LastName}" +
                                            $"|File:{correctionData.SRFileType}#{correctionData.UploadedFileName}");
                                    }
                                    else // (currentProfileRegisterPage2ID == correctionData.ProfileRegisterPage2ID) //Update the same ProfileRegisterPage2ID
                                    {
                                        currentProfileRegisterPage2ID = correctionData.ProfileRegisterPage2ID;

                                        propertyInfo.SetValue(toUpdateApplicantProfileRegisterPage2, "");
                                        _logger.LogInformation($"DataUpdate for Client:{client.ClientDeploymentID}-{client.ApplicationName} " +
                                            $"|ProfileRegisterPage2ID:{correctionData.ProfileRegisterPage2ID}| Applicant:{correctionData.ApplicantProfileID}#{toUpdateApplicantProfileRegisterPage2.ApplicantProfile.FirstName} " +
                                            $"{toUpdateApplicantProfileRegisterPage2.ApplicantProfile.MiddleName} {toUpdateApplicantProfileRegisterPage2.ApplicantProfile.OtherName} " +
                                            $"{toUpdateApplicantProfileRegisterPage2.ApplicantProfile.LastName}" +
                                            $"|File:{correctionData.SRFileType}#{correctionData.UploadedFileName}");
                                    }
                                }
                                else
                                {
                                    _logger.LogInformation($"ApplicantProfileID is 0 for Client:{client.ClientDeploymentID}-{client.ApplicationName} " +
                                      $"|ProfileRegisterPage2ID:{correctionData.ProfileRegisterPage2ID}");
                                }
                            }

                            //if (updateData.Equals("TRUE"))
                            //{
                            //    _logger.LogInformation($"Saving for Client:{client.ClientDeploymentID}-{client.ApplicationName}");
                            //    await _applicantProfileRegisterPage2Repository.SaveChangesAsync();
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation($"FAILED for Client:{client.ClientDeploymentID}-{client.ApplicationName}, WITH Message: {ex.Message}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"EXCEPTION Message: {ex.Message}");
                _logger.LogInformation($"EXCEPTION Type: {ex.GetType()}");
                _logger.LogInformation($"INNER Exceptio: {ex.InnerException}");
                _logger.LogInformation($"STACKTRACE: {ex.StackTrace}");
                return false;
            }
            return true;
        }
    }
}
