﻿using AttachmentsCorrection.Domain.Entities;
using AttachmentsCorrection.Domain.Entities.ATS;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Persistence
{
    public class AttachmentsCorrectionDbContext:DbContext
    {
        public AttachmentsCorrectionDbContext(DbContextOptions<AttachmentsCorrectionDbContext> options)
            :base(options)
        {

        }

        public DbSet<ApplicantProfile_ProfileRegisterPage2> applicantProfile_ProfileRegisterPage2 { get; set; }
        public DbSet<CorrectionData> correctionData { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AttachmentsCorrectionDbContext).Assembly);

            base.OnModelCreating(modelBuilder);
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //    => optionsBuilder.LogTo(Console.WriteLine);//.EnableSensitiveDataLogging();

    }
}
