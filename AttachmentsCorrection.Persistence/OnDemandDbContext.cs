﻿using AttachmentsCorrection.Domain.Entities.OnDemand;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Persistence
{
    public class OnDemandDbContext : DbContext
    {
        public OnDemandDbContext(DbContextOptions<OnDemandDbContext> options) : base(options)
        {
        }
        public DbSet<ClientDetail> ClientPreferences { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(OnDemandDbContext).Assembly);

            base.OnModelCreating(modelBuilder);
        }
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //    => optionsBuilder.LogTo(Console.WriteLine);//.EnableSensitiveDataLogging();
    }
}
