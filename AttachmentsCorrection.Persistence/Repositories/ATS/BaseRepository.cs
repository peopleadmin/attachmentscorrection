﻿using AttachmentsCorrection.Application.Contracts.Persistence;
using AttachmentsCorrection.Application.Contracts.Persistence.ATS;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AttachmentsCorrection.Persistence.Repositories.ATS
{
    public class BaseRepository<T> : IAsyncRepository<T> where T : class
    {
        protected readonly AttachmentsCorrectionDbContext _dbContext;

        public BaseRepository(AttachmentsCorrectionDbContext attachmentsCorrectionDbContext)
        {
           _dbContext = attachmentsCorrectionDbContext;
        }
        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public Task<IReadOnlyList<T>> ListAllAsync()
        {
            throw new NotImplementedException();
        }

        public void UpdateAsync(T entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
