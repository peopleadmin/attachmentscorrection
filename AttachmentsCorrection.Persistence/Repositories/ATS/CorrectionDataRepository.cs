﻿using AttachmentsCorrection.Application.Contracts.Persistence.ATS;
using AttachmentsCorrection.Domain.Entities.ATS;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttachmentsCorrection.Persistence.Repositories.ATS
{
    public class CorrectionDataRepository : BaseRepository<CorrectionData>, ICorrectionDataRepository
    {
        public CorrectionDataRepository(AttachmentsCorrectionDbContext dbContext) : base(dbContext)
        {
        }
        public async Task<List<CorrectionData>> GetCorrectionData()
        {
            var correctionData = await _dbContext.correctionData
                                        .FromSqlRaw($"SELECT AF.* FROM " +
                                        $"(SELECT ProfileRegisterPage2ID, ApplicantProfileID, FileType, UploadedFileName,	" +
                                        $"CASE WHEN FileType='ResumeName' THEN 'Applicant_Resume' " +
                                        $"					WHEN FileType='CoverLetterName' THEN 'Applicant_CoverLetter' " +
                                        $"					WHEN FileType='TranscriptsName' THEN 'Applicant_Transcript' " +
                                        $"					WHEN FileType='Custom1Name' THEN 'Applicant_Custom1' " +
                                        $"					WHEN FileType='Custom2Name' THEN 'Applicant_Custom2' " +
                                        $"					WHEN FileType='Custom3Name' THEN 'Applicant_Custom3' " +
                                        $"					END AS SRFileType			" +
                                        $"FROM ApplicantProfile_ProfileRegisterPage2					" +
                                        $"UNPIVOT					" +
                                        $"(						" +
                                        $"UploadedFileName					" +
                                        $"FOR FileType in (ResumeName, CoverLetterName, TranscriptsName, Custom1Name, Custom2Name, Custom3Name)				" +
                                        $") AS SchoolUnpivot 					 " +
                                        $"where ISNULL(UploadedFileName,'')<>''				" +
                                        $") AS AF		" +
                                        $"WHERE AF.SRFileType NOT IN " +
                                        $"(SELECT BF.SRFileType FROM BinaryFiles AS BF WHERE BF.ParentTable='ApplicantProfile_ProfileRegisterPage2'	" +
                                        $"AND BF.ParentID=AF.ProfileRegisterPage2ID	AND BF.AjaxFinalized='T')")
                                        .ToListAsync();

            return correctionData;
        }
    }
}
