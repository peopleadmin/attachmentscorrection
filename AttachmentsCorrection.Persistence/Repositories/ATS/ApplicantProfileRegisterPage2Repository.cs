﻿using AttachmentsCorrection.Application.Contracts.Persistence;
using AttachmentsCorrection.Application.Contracts.Persistence.ATS;
using AttachmentsCorrection.Domain.Entities;
using AttachmentsCorrection.Domain.Entities.ATS;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AttachmentsCorrection.Persistence.Repositories.ATS
{
    public class ApplicantProfileRegisterPage2Repository: BaseRepository<ApplicantProfile_ProfileRegisterPage2>, IApplicantProfileRegisterPage2Repository
    {
        public ApplicantProfileRegisterPage2Repository(AttachmentsCorrectionDbContext dbContext) : base(dbContext)
        {
        }
        public override async Task<ApplicantProfile_ProfileRegisterPage2> GetByIdAsync(int id)
        {
            var result = await _dbContext.applicantProfile_ProfileRegisterPage2
                .Where(i => i.ProfileRegisterPage2ID == id)
                .Include(ap => ap.ApplicantProfile)
                .FirstOrDefaultAsync();

            return result;
        }
    }
}
