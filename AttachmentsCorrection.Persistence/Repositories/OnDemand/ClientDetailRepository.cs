﻿using AttachmentsCorrection.Application.Contracts.Persistence.OnDemand;
using AttachmentsCorrection.Domain.Entities.OnDemand;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AttachmentsCorrection.Persistence.Repositories.OnDemand
{
    public class ClientDetailRepository : IClientDetailRepository
    {
        private readonly OnDemandDbContext _onDemandDbContext;

        public ClientDetailRepository(OnDemandDbContext onDemandDbContext)
        {
            _onDemandDbContext = onDemandDbContext;
        }

        public async Task<List<ClientDetail>> GetATSConnStrings(string getATSConnStringsSQL)
        {
            var clientDetails = await _onDemandDbContext.ClientPreferences
                            .FromSqlRaw(getATSConnStringsSQL)
                            .ToListAsync();

            return clientDetails;
        }
    }
}
