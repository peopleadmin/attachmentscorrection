﻿using AttachmentsCorrection.Domain.Entities.OnDemand;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AttachmentsCorrection.Persistence.Configurations
{
    public class ClientDetailConfiguration : IEntityTypeConfiguration<ClientDetail>
    {
        public void Configure(EntityTypeBuilder<ClientDetail> builder)
        {
            builder.HasNoKey();
        }
    }
}
