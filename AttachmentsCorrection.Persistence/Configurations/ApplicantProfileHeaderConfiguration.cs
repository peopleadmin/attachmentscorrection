﻿using AttachmentsCorrection.Domain.Entities.ATS;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Persistence.Configurations
{
    public class ApplicantProfileHeaderConfiguration : IEntityTypeConfiguration<ApplicantProfile_Header>
    {
        public void Configure(EntityTypeBuilder<ApplicantProfile_Header> builder)
        {
            builder.HasKey("ApplicantProfileID");
        }
    }
}
