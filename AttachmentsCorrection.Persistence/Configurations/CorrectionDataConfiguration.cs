﻿using AttachmentsCorrection.Domain.Entities.ATS;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AttachmentsCorrection.Persistence.Configurations
{
    public class CorrectionDataConfiguration : IEntityTypeConfiguration<CorrectionData>
    {
        public void Configure(EntityTypeBuilder<CorrectionData> builder)
        {
            builder.HasNoKey();
        }
    }
}
