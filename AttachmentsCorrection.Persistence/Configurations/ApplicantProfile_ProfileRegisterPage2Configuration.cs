﻿using AttachmentsCorrection.Domain.Entities.ATS;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace AttachmentsCorrection.Persistence.Configurations
{
    
    public class ApplicantProfile_ProfileRegisterPage2Configuration 
        : IEntityTypeConfiguration<ApplicantProfile_ProfileRegisterPage2>
    {
        public void Configure(EntityTypeBuilder<ApplicantProfile_ProfileRegisterPage2> builder)
        {
            builder.HasAnnotation("Table", "ApplicantProfile_ProfileRegisterPage2")
                .HasKey(j => j.ProfileRegisterPage2ID);
        }
    }
}
